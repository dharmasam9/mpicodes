/*
Author: Dharma teja
*/


#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>

int main(){
  double time_taken;
  int dict_size = 14;
  char* dict =  (char*)calloc(dict_size+1,sizeof(char));

  int rank;
  int np;

  // iniatializing MPI
  MPI_Init(NULL,NULL);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &np);

  MPI_Barrier(MPI_COMM_WORLD);
  time_taken -= MPI_Wtime();

  if(rank == 0){
	// MANAGER process

	// dictionary (can contain only small alphabets)
	char d[] = "abcdefghijklmn";
	int i,j;

	// sending dictionary to workers
	for(i=1;i<np;i++)
		MPI_Send(d, dict_size+1, MPI_CHAR, i, 0, MPI_COMM_WORLD);

	int** results;
	results = (int**)malloc(sizeof(int)*(np-1));
	for (i = 0; i < np; ++i)	results[i] = (int*) malloc(sizeof(int) * dict_size);

	// receiving results from workers
	for(i=1;i<np;i++){
		MPI_Recv(results[i-1], dict_size, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		// printf("[MANAGER] Received results from worker %d\n",i);
	}

	time_taken += MPI_Wtime();
	printf("%f time_taken\n",time_taken );

	/*
	// dictionary header
	for(j=0; j<dict_size; ++j)
		printf("%c ",d[j]);
		printf("\n");

	// worker results
	for (i = 0; i < np-1; ++i)
	{
		for(j=0; j<dict_size; ++j){
			printf("%d ",results[i][j]);
		}
		printf("filename(%d) \n",i+1);
	}
	*/
  }else{
	// we got the dictionary
	MPI_Recv(dict, dict_size+1, MPI_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	printf("[WORKER %d] Received dictionary %s from manager \n",rank, dict);
  
	// processor with rank i takes care of filename "i"
	char c = (char)((int)'0' + rank);
	char filename[2] = {c,'\0'};

	int flag[26] = {0};
	int count[26] = {0};

	int i;
	int offset = (int)'a';
	for (i = 0; i < dict_size; ++i)
	{
		flag[(int)dict[i] - offset] = 1;
	}

	FILE* fp = fopen(filename,"r");
	char ch;
	while(fscanf(fp,"%c",&ch) != EOF){
		if(flag[(int)ch - offset])
			count[(int)ch - offset] += 1;
	}

	fclose(fp);

	// sending results to rank 0
	int* results = (int*)calloc(dict_size,sizeof(int));
	int base = 0;
	for(i=0;i<26;i++){
		if(count[i] != 0){
			results[base] = count[i];
			base++;
		}
	}
 	
 	// sending results to manager
	MPI_Send(results, dict_size, MPI_INT, 0, 0, MPI_COMM_WORLD);	
	}

  MPI_Finalize();
return 0;
}