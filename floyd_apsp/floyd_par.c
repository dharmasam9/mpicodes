/*
Author: Dharma teja
*/


#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define BLOCK_LOW(pid, np, n) (pid)*(n)/(np)
#define BLOCK_HIGH(pid, np, n) (BLOCK_LOW(pid+1, np, n) - 1)
#define BLOCK_COUNT(pid, np, n) (BLOCK_LOW(pid+1, np, n) - BLOCK_LOW(pid, np, n))
#define BLOCK_OWNER(index, np, n) (((np*(index+1)) - 1)/n)


int min(a,b){
	if(a > b)
		return b;
	return a;
}


int main(){
	double time_taken;
	MPI_Init(NULL,NULL);

	int np;
	MPI_Comm_size(MPI_COMM_WORLD, &np);

	int pid;
	MPI_Comm_rank(MPI_COMM_WORLD, &pid);
	
	int row_start, row_end, row_count;

	int* mat_part;

	int i,j,k;
	int rows,cols;

	

	if(pid == np-1){
		
		// reading A matrix from the file
		FILE *fp;
		fp = fopen("A.mtx","r");

		fscanf(fp,"%d",&rows);
		fscanf(fp,"%d",&cols);

		// sending rows and cols to other processors
		for(i=0;i<np-1;i++){
			MPI_Send(&rows,1, MPI_INT, i, 0, MPI_COMM_WORLD);
			MPI_Send(&cols,1, MPI_INT, i, 1, MPI_COMM_WORLD);
		}

		int* mat = (int*) calloc(rows*cols, sizeof(int));

		int i,j;
		for(i=0;i<rows;i++){
			for(j=0;j<cols;j++){
				fscanf(fp, "%d", &mat[i*cols + j]);
			}
		}

		fclose(fp);

		MPI_Barrier(MPI_COMM_WORLD);
		time_taken -= MPI_Wtime();


		// sending blocks to corresponding processors
		int start,count;
		for(i=0;i<np-1;i++){
			start = BLOCK_LOW(i,np,rows);
			count = BLOCK_COUNT(i,np,rows);
			MPI_Send(&mat[start*cols],count*cols, MPI_INT,i, 2, MPI_COMM_WORLD );
		}


		// setting the row variables
		row_start = BLOCK_LOW(pid,np,rows);
		row_end = BLOCK_HIGH(pid,np,rows);
		row_count = BLOCK_COUNT(pid,np,rows);

		mat_part = (int*) calloc(row_count*cols, sizeof(int));
		
		for(i=0;i<row_count*cols;i++){
			mat_part[i] = mat[row_start*cols + i];
		}

		// printf("processor %d row_start %d row_end %d %d row_count\n",pid,row_start, row_end, row_count);
		
	}else{
		MPI_Recv(&rows,1, MPI_INT, np-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(&cols,1, MPI_INT, np-1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		MPI_Barrier(MPI_COMM_WORLD);
		time_taken -= MPI_Wtime();

		// setting the row variables
		row_start = BLOCK_LOW(pid,np,rows);
		row_end = BLOCK_HIGH(pid,np,rows);
		row_count = BLOCK_COUNT(pid,np,rows);

		mat_part = (int*) calloc(row_count*cols, sizeof(int));

		MPI_Recv(mat_part, row_count*cols, MPI_INT, np-1, 2, MPI_COMM_WORLD,MPI_STATUS_IGNORE);

		// printf("processor %d row_start %d row_end %d %d row_count\n",pid,row_start, row_end, row_count);		
		
	}

	

	// data is divided among processes
	// algorithm starts now

	int b_root;
	int* tmp_row;
	int row_offset;

	tmp_row = (int*) calloc(cols,sizeof(int));
	for(k=0;k<rows;k++){
		b_root = BLOCK_OWNER(k, np, rows);

		if(b_root == pid){
			row_offset = k - BLOCK_LOW(pid, np, rows);
			for(i=0;i<cols;i++){
				tmp_row[i] = mat_part[row_offset*cols + i];
			}
		}

		MPI_Bcast(tmp_row, cols, MPI_INT, b_root, MPI_COMM_WORLD);

		for(i=0;i< BLOCK_COUNT(pid, np, rows);i++){
			for(j=0;j<cols;j++){
				mat_part[i*cols + j] = min(mat_part[i*cols + j], mat_part[i*cols + k] + tmp_row[j]);
			}
		}
	}

	free(tmp_row);

	if(pid == -1){
		for(i=0;i<row_count;i++){
			for(j=0;j<cols;j++){
				printf("%d ",mat_part[i*cols + j]);
			}
			printf("\n");
		}
	}	


	if(pid == 0){
		
		// receive blocks from pid > 0
		int* tmp_block;
		int tmp_count;
		int tmp_row_start;

		int mat[rows*cols];

		for(i=1;i<np;i++){
			tmp_row_start = BLOCK_LOW(i,np,rows) * cols;
			tmp_count = BLOCK_COUNT(i,np,rows) * cols;
			tmp_block = (int*) calloc(tmp_count, sizeof(int));
			MPI_Recv(tmp_block, tmp_count, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			for(j=0;j<tmp_count;j++){
				mat[tmp_row_start + j] = tmp_block[j];
			}
		}

		tmp_row_start = BLOCK_LOW(pid,np,rows) * cols;
		tmp_count = BLOCK_COUNT(pid,np,rows) * cols;

		for(i=0;i<tmp_count;i++){
			mat[i] = mat_part[i];
		}

		time_taken += MPI_Wtime();
		printf("time taken %f\n",time_taken );	

		/*
		// printing the answer
		for(i=0;i<rows;i++){
			for(j=0;j<cols;j++)
				printf("%3d",mat[i*cols + j]);
			printf("\n");
		}
		*/

	}else{
		// send block to pid 0
		MPI_Send(mat_part, BLOCK_COUNT(pid, np, rows)*cols,MPI_INT,0,0,MPI_COMM_WORLD);
	}

	

	MPI_Finalize();


	return 0;
}