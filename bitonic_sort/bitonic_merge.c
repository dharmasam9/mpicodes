#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define BLOCK_LOW(pid, np, n) (pid)*(n)/(np)
#define BLOCK_HIGH(pid, np, n) (BLOCK_LOW(pid+1, np, n) - 1)
#define BLOCK_COUNT(pid, np, n) (BLOCK_LOW(pid+1, np, n) - BLOCK_LOW(pid, np, n))
#define BLOCK_OWNER(index, np, n) (((np*(index+1)) - 1)/n)

int LOG(int np){
	int ans = 0;
	while(np > 1){
		np /= 2;
		ans++;
	}
	return ans;
}
int BOX_OFFSET(int pid, int np, int level){
	int ans = 1;
	ans = ans << level;
	return ans;
}

int BOX_SIZE(int np,int level){
	return np >> level;
}

int main(){
	double time_taken;
	MPI_Init(NULL,NULL);

	int np;
	MPI_Comm_size(MPI_COMM_WORLD, &np);

	int pid;
	MPI_Comm_rank(MPI_COMM_WORLD, &pid);

	int i,j,k;
	int ele;
	int n;

	// read the elements and send them to other processors
	if(pid == np-1){

		FILE* fp;
		fp = fopen("input","r");
		fscanf(fp,"%d",&n);

		for(i=0;i<np-1;i++){
			MPI_Send(&n,1,MPI_INT, i, 0, MPI_COMM_WORLD);
		}

		if(n != np){
			printf("number of elements and processors should be same!\n");
			return 0;
		}

		int temp[n];
		for(i=0;i<n;i++){
			fscanf(fp,"%d",&temp[i]);
		}

		for(i=0;i<np-1;i++)
			MPI_Send(&temp[i],1,MPI_INT, i, 0, MPI_COMM_WORLD);

		fclose(fp);

		ele = temp[n-1];

	}else{

		MPI_Recv(&n,1,MPI_INT, np-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		if(n != np){
			return 0;
		}

		MPI_Recv(&ele,1,MPI_INT, np-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		
	}

	int l;
	int block_offset;

	for(l=0;l<LOG(np);l++){
		block_offset = pid - (pid/BOX_SIZE(np,l)) * BOX_SIZE(np,l);
		int corr_pid;
		int recv_ele;
		if(block_offset < BOX_SIZE(np,l+1)){
			corr_pid = pid + BOX_SIZE(np,l+1);
			MPI_Send(&ele,1,MPI_INT, corr_pid, 0, MPI_COMM_WORLD);
			MPI_Recv(&recv_ele,1,MPI_INT, corr_pid, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			// balancer upper end stores minimum
			if(ele > recv_ele){
				ele = recv_ele;
			}
		}else{
			corr_pid = pid - BOX_SIZE(np,l+1);
			MPI_Send(&ele,1,MPI_INT, corr_pid, 0, MPI_COMM_WORLD);
			MPI_Recv(&recv_ele,1,MPI_INT, corr_pid, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			// balancer lower end stores maximum
			if(ele < recv_ele){
				ele = recv_ele;
			}
		}
	}

	// accumulation of results at processor 0
	if(pid == 0){
		int values[n];

		values[0] = ele;
		for(i=1;i<np;i++)
			MPI_Recv(&values[i],1,MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		for(i=0;i<n;i++)
			printf("%3d", values[i]);
			printf("\n");
	}else{
		MPI_Send(&ele,1,MPI_INT,0, 0, MPI_COMM_WORLD);
	}

	MPI_Finalize();

	return 0;
}