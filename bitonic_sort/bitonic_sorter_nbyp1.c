#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int LOG(int np){
	int ans = 0;
	while(np > 1){
		np /= 2;
		ans++;
	}
	return ans;
}
int BOX_OFFSET(int pid, int np, int level){
	int ans = 1;
	ans = ans << level;
	return ans;
}

int BOX_SIZE(int np,int level){
	return np >> level;
}


int main(){
	double time_taken;
	MPI_Init(NULL,NULL);

	int np;
	MPI_Comm_size(MPI_COMM_WORLD, &np);

	int pid;
	MPI_Comm_rank(MPI_COMM_WORLD, &pid);

	int i,j,k;
	int ele;
	int n;

	// read the elements and send them to other processors
	if(pid == np-1){

		FILE* fp;
		fp = fopen("input","r");
		fscanf(fp,"%d",&n);

		for(i=0;i<np-1;i++){
			MPI_Send(&n,1,MPI_INT, i, 0, MPI_COMM_WORLD);
		}

		if(n != np){
			printf("number of elements and processors should be same!\n");
			return 0;
		}

		int temp[n];
		for(i=0;i<n;i++){
			fscanf(fp,"%d",&temp[i]);
		}

		for(i=0;i<np-1;i++)
			MPI_Send(&temp[i],1,MPI_INT, i, 0, MPI_COMM_WORLD);

		fclose(fp);

		ele = temp[n-1];

	}else{

		MPI_Recv(&n,1,MPI_INT, np-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		if(n != np){
			return 0;
		}

		MPI_Recv(&ele,1,MPI_INT, np-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		
	}

	
	int o_l, i_l;
	int g_bs, g_bn, g_bstrt, l_np, l_pid;

	for(o_l=LOG(np)-1 ;o_l>=0 ;o_l--){
		g_bs = BOX_SIZE(np,o_l);
		g_bn = pid/g_bs;
		g_bstrt = g_bn*g_bs;

		l_np = g_bs;
		l_pid = pid - g_bstrt;

				
		int l_offset;
		int l_strt;
		for(i_l=0; i_l<=LOG(l_np)-1; i_l++){
			l_offset = l_pid - (l_pid/BOX_SIZE(l_np,i_l)) * BOX_SIZE(l_np,i_l);
			l_strt = l_pid/BOX_SIZE(l_np,i_l) * BOX_SIZE(l_np,i_l);

			int corr_pid,recv_ele;
			if(l_offset < BOX_SIZE(l_np,i_l+1)){
				corr_pid = g_bstrt + l_strt + (l_offset + BOX_SIZE(l_np,i_l+1));

				MPI_Send(&ele,1,MPI_INT, corr_pid, 0, MPI_COMM_WORLD);
				MPI_Recv(&recv_ele,1,MPI_INT, corr_pid, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

				if(g_bn%2 == 0){
					// positive bitnoic sort
					if(ele > recv_ele){
						ele = recv_ele;
					}
				}else{
					//negative bitonic sort
					if(ele < recv_ele){
						ele = recv_ele;
					}

				}
			}else{
				corr_pid = g_bstrt + l_strt + (l_offset - BOX_SIZE(l_np,i_l+1));

				MPI_Send(&ele,1,MPI_INT, corr_pid, 0, MPI_COMM_WORLD);
				MPI_Recv(&recv_ele,1,MPI_INT, corr_pid, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

				if(g_bn%2 == 0){
					// positive bitnoic sort
					if(ele < recv_ele){
						ele = recv_ele;
					}
				}else{
					//negative bitonic sort
					if(ele > recv_ele){
						ele = recv_ele;
					}

				}
			}



		}


	}
	
	// accumulation of results at processor 0
	if(pid == 0){
		int values[n];

		values[0] = ele;
		for(i=1;i<np;i++)
			MPI_Recv(&values[i],1,MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		for(i=0;i<n;i++)
			printf("%3d", values[i]);
			printf("\n");
	}else{
		MPI_Send(&ele,1,MPI_INT,0, 0, MPI_COMM_WORLD);
	}
	
	
	MPI_Finalize();
	return 0;
}