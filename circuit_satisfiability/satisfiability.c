/*
Author: Dharma teja
*/

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>


int get_last_n_bits(int num,int n,int* bits){
	while(n >= 0){
		bits[n-1] = num%2;
		num /= 2;
		n--;
	}
}


int check_circuit(int num, int num_bits){
	int i;
	int* bits = (int*) malloc(sizeof(int)* num_bits);

	get_last_n_bits(num,num_bits,bits);

	if(( (bits[num_bits-4] && bits[num_bits-3]) || bits[num_bits-2]) && bits[num_bits-1]){
		for(i=0;i<num_bits;i++)
			printf("%d ",bits[i]);
			printf("\n");
		return 1;
	}
	return 0;
}


int main(int argc,int* argv){
	double time_taken;
	MPI_Init(NULL,NULL);

	MPI_Barrier(MPI_COMM_WORLD);
	time_taken -= MPI_Wtime();

	int num_procs;
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	int proc_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);

	int i,j;
	int num_bits = argv[2];
	printf("%d\n",num_bits );
	int all_ones = 1;

	// finding the large possible value
	for(i=0;i<num_bits;i++)
	 	all_ones *= 2;

	int local_count = 0;
	int global_count = 0;

	for(i=proc_rank;i<=all_ones;i=i+num_procs){
		local_count += check_circuit(i,num_bits);
	}

	MPI_Reduce(&local_count, &global_count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

	time_taken += MPI_Wtime();

	MPI_Finalize();

	if(proc_rank == 0){
		printf("Total number of solutions are %d\n", global_count);
		printf("total time taken %f\n",time_taken);
	}

	return 0;
}