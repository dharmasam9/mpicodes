/*
Author: Dharma teja
*/


#include <stdio.h>
#include <stdlib.h>

// circuit
/*
	(((a && b) || c) && d)
*/

int get_last_n_bits(int num,int n,int* bits){
	while(n >= 0){
		bits[n-1] = num%2;
		num /= 2;
		n--;
	}
}


void check_circuit(int num, int num_bits){
	int i;
	int* bits = (int*) malloc(sizeof(int)* num_bits);

	get_last_n_bits(num,num_bits,bits);

	if(( (bits[0] && bits[1]) || bits[2]) && bits[3]){
		for(i=0;i<4;i++)
			printf("%d ",bits[i]);
			printf("\n");
	}
}

int main(){
	int i,j;
	int num_bits = 4;
	int all_ones = 1;

	// finding the large possible value
	for(i=0;i<4;i++)
	 	all_ones *= 2;

	for(i=0;i<all_ones;i++){
		check_circuit(i,num_bits);
	}

	return 0;
}