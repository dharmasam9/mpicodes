/*
Author: Dharma teja
*/


#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define BLOCK_LOW(pid, np, n) (pid)*(n)/(np)
#define BLOCK_HIGH(pid, np, n) (BLOCK_LOW(pid+1, np, n) - 1)
#define BLOCK_COUNT(pid, np, n) (BLOCK_LOW(pid+1, np, n) - BLOCK_LOW(pid, np, n))
#define BLOCK_OWNER(index, np, n) (((np*(index+1)) - 1)/n)


int main(){
	double time_taken;
	MPI_Init(NULL,NULL);

	int np;
	MPI_Comm_size(MPI_COMM_WORLD, &np);

	int pid;
	MPI_Comm_rank(MPI_COMM_WORLD, &pid);
	
	int row_start, row_end, row_count;

	int* mat_part;
	int* vec;

	int i,j,k;
	int rows,cols;

	if(pid == np-1){
		
		// reading A matrix from the file
		FILE *fp;
		fp = fopen("A.mtx","r");

		fscanf(fp,"%d",&rows);
		fscanf(fp,"%d",&cols);

		// sending rows and cols to other processors
		for(i=0;i<np-1;i++){
			MPI_Send(&rows,1, MPI_INT, i, 0, MPI_COMM_WORLD);
			MPI_Send(&cols,1, MPI_INT, i, 1, MPI_COMM_WORLD);
		}

		int* mat = (int*) calloc(rows*cols, sizeof(int));

		int i,j;
		for(i=0;i<rows;i++){
			for(j=0;j<cols;j++){
				fscanf(fp, "%d", &mat[i*cols + j]);
			}
		}

		fclose(fp);

		// reading vector B
		int B_rows;

		fp = fopen("B.mtx","r");
		fscanf(fp,"%d",&B_rows);

		vec = (int*) calloc(B_rows, sizeof(int));

		for(i=0;i<cols;i++){
			fscanf(fp,"%d",&vec[i]);
		}

		fclose(fp);

		MPI_Barrier(MPI_COMM_WORLD);
		time_taken -= MPI_Wtime();

		// sending blocks to corresponding processors
		int start,count;
		for(i=0;i<np-1;i++){
			start = BLOCK_LOW(i,np,rows);
			count = BLOCK_COUNT(i,np,rows);
			MPI_Send(&mat[start*cols],count*cols, MPI_INT,i, 2, MPI_COMM_WORLD );
		}


		// setting the row variables
		row_start = BLOCK_LOW(pid,np,rows);
		row_end = BLOCK_HIGH(pid,np,rows);
		row_count = BLOCK_COUNT(pid,np,rows);

		mat_part = (int*) calloc(row_count*cols, sizeof(int));
		
		for(i=0;i<row_count*cols;i++){
			mat_part[i] = mat[row_start*cols + i];
		}

		//printf("processor %d row_start %d row_end %d %d row_count\n",pid,row_start, row_end, row_count);
		
	}else{
		MPI_Recv(&rows,1, MPI_INT, np-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(&cols,1, MPI_INT, np-1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		MPI_Barrier(MPI_COMM_WORLD);
		time_taken -= MPI_Wtime();

		// setting the row variables
		row_start = BLOCK_LOW(pid,np,rows);
		row_end = BLOCK_HIGH(pid,np,rows);
		row_count = BLOCK_COUNT(pid,np,rows);

		mat_part = (int*) calloc(row_count*cols, sizeof(int));

		MPI_Recv(mat_part, row_count*cols, MPI_INT, np-1, 2, MPI_COMM_WORLD,MPI_STATUS_IGNORE);

		//printf("processor %d row_start %d row_end %d %d row_count\n",pid,row_start, row_end, row_count);		
		
	}



	// broadcasting vector to all processors	
	if(pid == np-1){
		MPI_Bcast(vec, cols, MPI_INT, pid, MPI_COMM_WORLD);
	}else{
		vec = (int*) calloc(cols, sizeof(int)); // allocating for other processors
		MPI_Bcast(vec, cols, MPI_INT, np-1, MPI_COMM_WORLD);
	}

	
	// doing computation in parallel

	int* res = (int*) calloc(row_count,sizeof(int));

	for(i=0;i<row_count;i++){
		for(j=0;j<cols;j++){
			res[i] += mat_part[i*cols+j] * vec[j];
		}
	}


	// accumulating answer at pid 0
	if(pid == 0){
		int fin_ans[rows];
		int* tmp_block;
		int tmp_count;

		for(i=1;i<np;i++){
			tmp_count = BLOCK_COUNT(i,np,rows);
			tmp_block = (int*) malloc(tmp_count*sizeof(int));
			MPI_Recv(tmp_block, tmp_count, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			for(j=0;j<tmp_count;j++){
				fin_ans[BLOCK_LOW(i,np,rows)+j] = tmp_block[j];
			}

		}

		// setting zero values
		tmp_count = BLOCK_COUNT(pid,np,rows);
		for(i=0;i<tmp_count;i++)
			fin_ans[i] = res[i];

		time_taken += MPI_Wtime();

		printf("time taken is %f\n",time_taken );

		/*
		for (i = 0; i < rows; i++)
			printf("%d\n",fin_ans[i]);
		*/

	}else{
		// send the result
		MPI_Send(res, row_count, MPI_INT, 0, 0, MPI_COMM_WORLD);
	}




	return 0;
}